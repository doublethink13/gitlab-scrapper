from sqlalchemy import Column
from sqlalchemy_redshift.dialect import INTEGER, VARCHAR

from app.redshift_tables.base import Base


class GitLabUsers(Base):
    __tablename__ = "gitlab_users"

    hash = Column(VARCHAR(256), primary_key=True)
    timestamp = Column(INTEGER)

    id = Column(INTEGER)
    name = Column(VARCHAR(128))
    username = Column(VARCHAR(128))
    state = Column(VARCHAR(64))
    avatar_url = Column(VARCHAR(256))
    web_url = Column(VARCHAR(256))
