from sqlalchemy import Column
from sqlalchemy_redshift.dialect import BOOLEAN, DECIMAL, INTEGER, TIMESTAMPTZ, VARCHAR

from app.redshift_tables.base import Base


class GitLabPipelines(Base):
    __tablename__ = "gitlab_pipelines"

    hash = Column(VARCHAR(128), primary_key=True)
    timestamp = Column(INTEGER)

    id = Column(INTEGER)
    iid = Column(INTEGER)
    project_id = Column(INTEGER)
    status = Column(VARCHAR(128))
    ref = Column(VARCHAR(128))
    sha = Column(VARCHAR(128))
    before_sha = Column(VARCHAR(128))
    tag = Column(BOOLEAN, default=False)
    yaml_errors = Column(VARCHAR(256), nullable=True)
    user = Column(INTEGER)
    created_at = Column(TIMESTAMPTZ)
    updated_at = Column(TIMESTAMPTZ)
    started_at = Column(TIMESTAMPTZ, nullable=True)
    finished_at = Column(TIMESTAMPTZ)
    committed_at = Column(TIMESTAMPTZ, nullable=True)
    duration = Column(DECIMAL, nullable=True)
    queued_duration = Column(DECIMAL, nullable=True)
    coverage = Column(VARCHAR(128), nullable=True)
    web_url = Column(VARCHAR(256))
