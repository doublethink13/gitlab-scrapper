from pathlib import Path

from pydantic import BaseSettings, Field

from app.types.metrics import Metrics


class Settings(BaseSettings):
    aws_access_key_id: str = Field(..., env="AWS_ACCESS_KEY_ID")
    aws_secret_access_id: str = Field(..., env="AWS_SECRET_ACCESS_KEY")
    aws_default_region: str = Field(..., env="AWS_DEFAULT_REGION")
    aws_bucket_name: str = Field(..., env="AWS_BUCKET_NAME")

    gitlab_url: str = Field(..., env="GITLAB_URL")
    gitlab_private_token: str | None = Field(env="GITLAB_PRIVATE_TOKEN", default=None)
    gitlab_oauth_token: str | None = Field(env="GITLAB_OAUTH_TOKEN", default=None)
    gitlab_job_token: str | None = Field(env="GITLAB_JOB_TOKEN", default=None)
    gitlab_projects_ids: set[str] = Field(env="GITLAB_PROJECTS_IDS", default=set[str]())
    gitlab_owned: bool | None = Field(env="GITLAB_OWNED", default=True)
    gitlab_allowed_metrics: set[Metrics] = Field(
        env="GITLAB_ALLOWED_METRICS", default=set[Metrics]()
    )

    redshift_username: str = Field(..., env="REDSHIFT_USERNAME")
    redshift_password: str = Field(..., env="REDSHIFT_PASSWORD")
    redshift_engine: str = Field(..., env="REDSHIFT_ENGINE")
    redshift_host: str = Field(..., env="REDSHIFT_HOST")
    redshift_port: int = Field(..., env="REDSHIFT_PORT")
    redshift_db_cluster_identifier: str | None = Field(
        ..., env="REDSHIFT_DB_CLUSTER_IDENTIFIER"
    )
    redshift_database: str = Field(..., env="REDSHIFT_DATABASE")

    class Config:  # type:ignore
        env_file = f"{Path(__file__).parent.parent.parent.resolve()}/.env"
        env_file_encoding = "utf-8"

        @classmethod
        def parse_env_var(cls, field_name: str, raw_value: str) -> set[str]:
            if field_name in {"gitlab_projects_ids", "gitlab_allowed_metrics"}:
                return set(raw_value.split(","))

            return cls.json_loads(raw_value)  # type:ignore
