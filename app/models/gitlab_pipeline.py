from datetime import datetime
from typing import Any

from deepdiff import DeepHash
from pandas import (
    BooleanDtype,
    DataFrame,
    DatetimeTZDtype,
    Float64Dtype,
    Int64Dtype,
    StringDtype,
    to_datetime,
)

from app.models.custom_base_model import CustomBaseModel
from app.types.gitlab_datetime_format import GITLAB_DATETIME_FORMAT


class GitLabPipeline(CustomBaseModel):
    id: int
    iid: int
    project_id: int
    status: str
    ref: str
    sha: str
    before_sha: str
    tag: bool
    yaml_errors: str | None
    user: int
    created_at: datetime
    updated_at: datetime
    started_at: datetime | None
    finished_at: datetime
    committed_at: datetime | None
    duration: float | None
    queued_duration: float | None
    coverage: str | None
    web_url: str

    hash: str
    timestamp: int

    @classmethod
    def parse_api_response(
        cls, response: dict[str, Any], timestamp: int
    ) -> "GitLabPipeline":
        return GitLabPipeline(
            id=response["id"],
            iid=response["iid"],
            project_id=response["project_id"],
            status=response["status"],
            ref=response["ref"],
            sha=response["sha"],
            before_sha=response["before_sha"],
            tag=response["tag"],
            yaml_errors=response["yaml_errors"],
            user=response["user"]["id"],
            created_at=datetime.strptime(
                response["created_at"], GITLAB_DATETIME_FORMAT
            ),
            updated_at=datetime.strptime(
                response["updated_at"], GITLAB_DATETIME_FORMAT
            ),
            started_at=datetime.strptime(response["started_at"], GITLAB_DATETIME_FORMAT)
            if response["started_at"]
            else None,
            finished_at=datetime.strptime(
                response["finished_at"], GITLAB_DATETIME_FORMAT
            ),
            committed_at=datetime.strptime(
                response["committed_at"], GITLAB_DATETIME_FORMAT
            )
            if response["committed_at"]
            else None,
            duration=response["duration"],
            queued_duration=response["queued_duration"],
            coverage=response["coverage"],
            web_url=response["web_url"],
            hash=str(DeepHash(response)[response]),  # type:ignore
            timestamp=timestamp,
        )

    @classmethod
    def generate_dataframe(cls, gitlab_pipelines: list["GitLabPipeline"]) -> DataFrame:
        dataframe = DataFrame(
            [
                GitLabPipeline._to_pandas_record(gitlab_pipeline)
                for gitlab_pipeline in gitlab_pipelines
            ]
        )

        dataframe = dataframe.drop_duplicates(subset="hash")

        current_timezone = datetime.now().astimezone().tzinfo

        return dataframe.astype(
            {
                "id": Int64Dtype(),
                "iid": Int64Dtype(),
                "project_id": Int64Dtype(),
                "status": StringDtype(),
                "ref": StringDtype(),
                "sha": StringDtype(),
                "before_sha": StringDtype(),
                "tag": BooleanDtype(),
                "yaml_errors": StringDtype(),
                "user": Int64Dtype(),
                "created_at": DatetimeTZDtype(tz=current_timezone),
                "updated_at": DatetimeTZDtype(tz=current_timezone),
                "started_at": DatetimeTZDtype(tz=current_timezone),
                "finished_at": DatetimeTZDtype(tz=current_timezone),
                "committed_at": DatetimeTZDtype(tz=current_timezone),
                "duration": Float64Dtype(),
                "queued_duration": Float64Dtype(),
                "coverage": StringDtype(),
                "web_url": StringDtype(),
                "hash": StringDtype(),
                "timestamp": Int64Dtype(),
            }
        )

    @classmethod
    def _to_pandas_record(cls, gitlab_pipeline: "GitLabPipeline") -> dict[str, Any]:
        return {
            "id": gitlab_pipeline.id,
            "iid": gitlab_pipeline.iid,
            "project_id": gitlab_pipeline.project_id,
            "status": gitlab_pipeline.status,
            "ref": gitlab_pipeline.ref,
            "sha": gitlab_pipeline.sha,
            "before_sha": gitlab_pipeline.before_sha,
            "tag": gitlab_pipeline.tag,
            "yaml_errors": gitlab_pipeline.yaml_errors,
            "user": gitlab_pipeline.user,
            "created_at": to_datetime(gitlab_pipeline.created_at),
            "updated_at": to_datetime(gitlab_pipeline.updated_at),
            "started_at": to_datetime(gitlab_pipeline.started_at)
            if gitlab_pipeline.started_at
            else None,
            "finished_at": to_datetime(gitlab_pipeline.finished_at),
            "committed_at": to_datetime(gitlab_pipeline.committed_at)
            if gitlab_pipeline.committed_at
            else None,
            "duration": gitlab_pipeline.duration,
            "queued_duration": gitlab_pipeline.queued_duration,
            "coverage": gitlab_pipeline.coverage,
            "web_url": gitlab_pipeline.web_url,
            "hash": gitlab_pipeline.hash,
            "timestamp": gitlab_pipeline.timestamp,
        }
