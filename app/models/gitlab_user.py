from typing import Any

from deepdiff import DeepHash
from pandas import DataFrame, Int64Dtype, StringDtype

from app.models.custom_base_model import CustomBaseModel


# TODOs:
# restrict possible value where possible (for example, state can be active, etc.. only)
# create abstract class -> interface for gitlab resources
class GitLabUser(CustomBaseModel):
    id: int
    name: str
    username: str
    state: str
    avatar_url: str
    web_url: str

    hash: str
    timestamp: int

    @classmethod
    def parse_api_response(
        cls, response: dict[str, Any], timestamp: int
    ) -> "GitLabUser":
        return GitLabUser(
            id=response["id"],
            name=response["name"],
            username=response["username"],
            state=response["state"],
            avatar_url=response["avatar_url"],
            web_url=response["web_url"],
            hash=str(DeepHash(response)[response]),  # type:ignore
            timestamp=timestamp,
        )

    @classmethod
    def generate_dataframe(cls, gitlab_users: list["GitLabUser"]) -> DataFrame:
        dataframe = DataFrame(
            [GitLabUser.to_pandas_record(gitlab_user) for gitlab_user in gitlab_users]
        )

        dataframe.drop_duplicates(subset="hash", inplace=True)

        return dataframe.astype(
            {
                "id": Int64Dtype(),
                "name": StringDtype(),
                "username": StringDtype(),
                "state": StringDtype(),
                "avatar_url": StringDtype(),
                "web_url": StringDtype(),
                "hash": StringDtype(),
                "timestamp": Int64Dtype(),
            }
        )

    @classmethod
    def to_pandas_record(cls, gitlab_user: "GitLabUser") -> dict[str, Any]:
        return gitlab_user.dict()
