from pydantic import BaseModel

from app.types.parsed_keys import ParsedKeys


class ObjectInformation(BaseModel):
    most_recent_timestamp: int
    parsed_keys: ParsedKeys
