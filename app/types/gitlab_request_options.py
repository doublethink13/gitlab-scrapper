from typing import TypedDict

from typing_extensions import NotRequired


class GitLabRequestOptions(TypedDict):
    owned: NotRequired[bool | None]
