from app.types.filenames import Filenames
from app.types.timestamp import Timestamp

ParsedKeys = dict[Timestamp, Filenames]
