from dagster import Definitions

from app.assets.gitlab_pipelines_asdict import gitlab_pipelines_asdict
from app.assets.gitlab_pipelines_dataframe import gitlab_pipelines_dataframe
from app.assets.gitlab_pipelines_parquet_uri import gitlab_pipelines_parquet_uri
from app.assets.gitlab_pipelines_table import gitlab_pipelines_table
from app.assets.gitlab_users_dataframe import gitlab_users_dataframe
from app.assets.gitlab_users_parquet_uri import gitlab_users_parquet_uri
from app.assets.gitlab_users_table import gitlab_users_table
from app.assets.timestamp import timestamp
from app.resources.database import Database
from app.resources.gitlab_api import GitLabApi
from app.resources.s3_api import S3Api

DEFINITIONS = Definitions(
    assets=[
        timestamp,
        gitlab_pipelines_asdict,
        gitlab_users_dataframe,
        gitlab_pipelines_dataframe,
        gitlab_users_parquet_uri,
        gitlab_pipelines_parquet_uri,
        gitlab_users_table,
        gitlab_pipelines_table,
    ],
    resources={
        "database": Database.configure_at_launch(),
        "gitlab": GitLabApi.configure_at_launch(),
        "s3": S3Api.configure_at_launch(),
    },
)
