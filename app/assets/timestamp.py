from datetime import datetime

from dagster import asset


@asset
def timestamp() -> int:
    return int(datetime.now().timestamp())
