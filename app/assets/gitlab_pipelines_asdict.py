from dagster import Any, OpExecutionContext, asset
from gitlab.v4.objects import ProjectPipeline

from app.resources.gitlab_api import GitLabApi
from app.utils.get_all_gitlab_resources import get_all_gitlab_resources


@asset
def gitlab_pipelines_asdict(
    context: OpExecutionContext, gitlab: GitLabApi
) -> list[dict[str, Any]]:
    gitlab_pipelines: list[dict[str, Any]] = []

    for project in gitlab.projects:
        simple_pipelines: list[ProjectPipeline] = get_all_gitlab_resources(
            project.pipelines
        )

        # TODO: handle pipelines without id; is that even possible?
        for simple_pipeline in simple_pipelines:
            if pipeline_id := simple_pipeline.get_id():
                context.log.debug(f"Loading pipeline '{pipeline_id}'")

                gitlab_pipelines.append(project.pipelines.get(pipeline_id).asdict())

    return gitlab_pipelines
