from dagster import Any, OpExecutionContext, asset
from pandas import DataFrame

from app.models.gitlab_user import GitLabUser
from app.utils.generate_dataframe import generate_dataframe


@asset
def gitlab_users_dataframe(
    context: OpExecutionContext,
    timestamp: int,
    gitlab_pipelines_asdict: list[dict[str, Any]],
) -> DataFrame:
    return generate_dataframe(
        context=context,
        model=GitLabUser,
        timestamp=timestamp,
        asdicts=[asdict["user"] for asdict in gitlab_pipelines_asdict],
    )
