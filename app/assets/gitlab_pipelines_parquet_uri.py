import awswrangler as wr
from dagster import asset
from pandas import DataFrame

from app.resources.s3_api import S3Api
from app.utils.upload_dataframe_as_parquet_to_s3 import (
    upload_dataframe_as_parquet_to_s3,
)


@asset
def gitlab_pipelines_parquet_uri(
    timestamp: int, gitlab_pipelines_dataframe: DataFrame, s3: S3Api
) -> str:
    return upload_dataframe_as_parquet_to_s3(
        bucket_name=s3.bucket.name,
        timestamp=timestamp,
        model="pipelines",
        dataframe=gitlab_pipelines_dataframe,
    )
