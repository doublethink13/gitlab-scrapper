from dagster import asset

from app.redshift_tables.gitlab_pipelines import GitLabPipelines
from app.resources.database import Database
from app.utils.update_redshift_table import update_redshift_table


@asset
def gitlab_pipelines_table(
    gitlab_pipelines_parquet_uri: str, database: Database
) -> None:
    update_redshift_table(gitlab_pipelines_parquet_uri, database, GitLabPipelines)
