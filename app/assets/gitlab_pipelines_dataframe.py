from dagster import Any, OpExecutionContext, asset
from pandas import DataFrame

from app.models.gitlab_pipeline import GitLabPipeline
from app.utils.generate_dataframe import generate_dataframe


@asset
def gitlab_pipelines_dataframe(
    context: OpExecutionContext,
    timestamp: int,
    gitlab_pipelines_asdict: list[dict[str, Any]],
) -> DataFrame:
    return generate_dataframe(
        context=context,
        model=GitLabPipeline,
        timestamp=timestamp,
        asdicts=gitlab_pipelines_asdict,
    )
