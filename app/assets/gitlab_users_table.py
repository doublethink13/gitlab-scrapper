from dagster import asset

from app.redshift_tables.gitlab_users import GitLabUsers
from app.resources.database import Database
from app.utils.update_redshift_table import update_redshift_table


@asset
def gitlab_users_table(gitlab_users_parquet_uri: str, database: Database) -> None:
    update_redshift_table(gitlab_users_parquet_uri, database, GitLabUsers)
