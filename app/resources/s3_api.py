from typing import Any

from boto3 import Session
from dagster import ConfigurableResource
from mypy_boto3_s3.service_resource import Bucket

from app.models.settings import Settings


class S3Api(ConfigurableResource):
    access_key_id: str | None
    secret_access_id: str | None
    default_region: str | None
    bucket_name: str | None

    def __init__(self, **data: Any):
        super().__init__(**data)

        self._settings = Settings()

        self._session = Session().resource("s3")

        self._bucket = self._session.Bucket(
            self.bucket_name or self._settings.aws_bucket_name
        )

    @property
    def bucket(self) -> Bucket:
        return self._bucket

    def upload_file(self, filename: str, object_key: str) -> str:
        self.bucket.upload_file(filename, object_key)

        return f"s3://{self.bucket.name}/{object_key}"

    def load_bucket_keys(self) -> list[str]:
        return [bucket.key for bucket in self._bucket.objects.all()]
