from typing import Any

from dagster import ConfigurableResource
from gitlab import Gitlab
from gitlab.v4.objects import Project

from app.models.settings import Settings
from app.utils.get_all_gitlab_resources import get_all_gitlab_resources


class GitLabApi(ConfigurableResource):
    gitlab_url: str | None
    gitlab_private_token: str | None
    gitlab_oauth_token: str | None
    gitlab_job_token: str | None
    gitlab_projects_ids: list[str] | None
    gitlab_owned: bool | None
    gitlab_allowed_metrics: list[str] | None

    def __init__(self, **data: Any):
        super().__init__(**data)

        self._settings = Settings()

        self._api = self._load_api()

        self._projects = self._load_projects()

    @property
    def api(self) -> Gitlab:
        return self._api

    @property
    def projects(self) -> list[Project]:
        return self._projects

    def _load_api(self) -> Gitlab:
        gl = Gitlab(
            self.gitlab_url or self._settings.gitlab_url,
            private_token=self.gitlab_private_token
            or self._settings.gitlab_private_token,
            oauth_token=self.gitlab_oauth_token or self._settings.gitlab_oauth_token,
            job_token=self.gitlab_job_token or self._settings.gitlab_job_token,
            order_by="id",
            per_page=20,
        )

        gl.auth()

        return gl

    def _load_projects(self) -> list[Project]:
        projects_ids = self.gitlab_projects_ids or self._settings.gitlab_projects_ids
        assert projects_ids is not None

        if len(projects_ids):
            projects = [
                self._api.projects.get(project_id) for project_id in projects_ids
            ]
        else:
            projects = get_all_gitlab_resources(
                self._api.projects,
                owned=self.gitlab_owned or self._settings.gitlab_owned,
            )

        return projects
