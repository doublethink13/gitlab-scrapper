from contextlib import contextmanager
from typing import Any, Generator

from dagster import ConfigurableResource
from sqlalchemy.engine import URL, Connection, Engine, create_engine
from sqlalchemy.orm import Session

from app.models.settings import Settings


class Database(ConfigurableResource):
    username: str | None
    password: str | None
    engine: str | None
    host: str | None
    port: int | None
    db_cluster_identifier: str | None
    database: str | None

    def __init__(self, **data: Any):
        super().__init__(**data)

        self._env_settings = Settings()

        self._engine = self._create_engine()

    @contextmanager
    def session(self) -> Generator[Session, None, None]:
        with Session(bind=self._engine) as session, session.begin():
            try:
                yield session
            finally:
                ...

    @contextmanager
    def connection(self) -> Generator[Connection, None, None]:
        with self._engine.connect() as connection:
            try:
                yield connection
            finally:
                ...

    def ensure_table(self, table: Any) -> None:
        table.__table__.create(bind=self._engine, checkfirst=True)

    def _create_engine(self) -> Engine:
        connect_args = {"connect_timeout": 60 * 60 * 16, "sslmode": "verify-ca"}

        url = URL.create(
            drivername=f"{self.engine or self._env_settings.redshift_engine}+psycopg2",
            username=f"{self.username or self._env_settings.redshift_username}",
            password=f"{self.password or self._env_settings.redshift_password}",
            host=f"{self.host or self._env_settings.redshift_host}",
            port=self.port or self._env_settings.redshift_port,
            database=f"{self.database or self._env_settings.redshift_database}",
        )

        return create_engine(url, connect_args=connect_args)
