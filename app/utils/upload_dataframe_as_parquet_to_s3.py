import awswrangler as wr
from pandas import DataFrame


def upload_dataframe_as_parquet_to_s3(
    bucket_name: str, timestamp: int, model: str, dataframe: DataFrame
) -> str:
    uri = f"s3://{bucket_name}/{timestamp}/{model}.parquet"

    wr.s3.to_parquet(dataframe, path=uri)

    return uri
