from typing import Any

from dagster import OpExecutionContext
from pandas import DataFrame

from app.models.gitlab_pipeline import GitLabPipeline
from app.models.gitlab_user import GitLabUser

GitLabModels = GitLabPipeline | GitLabUser


def generate_dataframe(
    context: OpExecutionContext,
    model: GitLabModels,
    timestamp: int,
    asdicts: list[dict[str, Any]],
) -> DataFrame:
    models: list[GitLabModels] = [
        model.parse_api_response(asdict, timestamp) for asdict in asdicts
    ]

    dataframe = model.generate_dataframe(models)

    context.log.debug(f"Generated dataframe with schema:\n{dataframe.dtypes}")

    return dataframe
