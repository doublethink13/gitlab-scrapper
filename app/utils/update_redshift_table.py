from awswrangler import s3

from app.redshift_tables.gitlab_pipelines import GitLabPipelines
from app.redshift_tables.gitlab_users import GitLabUsers
from app.resources.database import Database
from app.utils.generate_sqlalchemy_rows_from_dataframe import (
    generate_sqlalchemy_rows_from_dataframe,
)


# TODO: missing tests, needs edbintt for sqlalcemy 1.4
def update_redshift_table(
    gitlab_pipelines_parquet_uri: str,
    database: Database,
    table: GitLabPipelines | GitLabUsers,
) -> None:
    database.ensure_table(table)

    dataframe = s3.read_parquet(gitlab_pipelines_parquet_uri)

    rows, hashes = generate_sqlalchemy_rows_from_dataframe(dataframe, table)

    with database.session() as session:
        session.query(table).where(table.hash.in_(hashes)).delete()

    with database.session() as session:
        session.add_all(rows)
