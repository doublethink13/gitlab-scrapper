from typing import overload

from numpy import nan
from pandas import DataFrame, NaT

from app.redshift_tables.gitlab_pipelines import GitLabPipelines
from app.redshift_tables.gitlab_users import GitLabUsers


@overload
def generate_sqlalchemy_rows_from_dataframe(
    dataframe: DataFrame, row: GitLabUsers
) -> tuple[list[GitLabUsers], list[str]]:
    ...


@overload
def generate_sqlalchemy_rows_from_dataframe(
    dataframe: DataFrame, row: GitLabPipelines
) -> tuple[list[GitLabPipelines], list[str]]:
    ...


def generate_sqlalchemy_rows_from_dataframe(
    dataframe: DataFrame, row: GitLabUsers | GitLabPipelines
) -> tuple[list[GitLabUsers], list[str]] | tuple[list[GitLabPipelines], list[str]]:
    column_names = row.__table__.columns.keys()

    rows = []
    hashes = []

    dataframe.replace({nan: None, NaT: None}, inplace=True)

    for _, dataframe_row in dataframe.iterrows():
        args = {column_name: dataframe_row[column_name] for column_name in column_names}

        rows.append(row(**args))
        hashes.append(dataframe_row["hash"])

    return rows, hashes
