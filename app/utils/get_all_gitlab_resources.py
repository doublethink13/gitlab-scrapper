from typing import Any, overload

from gitlab.v4.objects import (
    Project,
    ProjectManager,
    ProjectPipeline,
    ProjectPipelineManager,
)
from typing_extensions import Unpack

from app.types.gitlab_request_options import GitLabRequestOptions


@overload
def get_all_gitlab_resources(
    gitlab_resource_manager: ProjectManager,
    **kwargs: Unpack[GitLabRequestOptions],  # type:ignore
) -> list[Project]:
    ...


@overload
def get_all_gitlab_resources(
    gitlab_resource_manager: ProjectPipelineManager,
    **kwargs: Unpack[GitLabRequestOptions],  # type:ignore
) -> list[ProjectPipeline]:
    ...


def get_all_gitlab_resources(
    gitlab_resource_manager: ProjectManager | ProjectPipelineManager,
    **kwargs: Unpack[GitLabRequestOptions],  # type:ignore
) -> list[Project] | list[ProjectPipeline]:
    projects: list[Any] = []

    page = 1
    while True:
        current_page_projects = gitlab_resource_manager.list(page=page, **kwargs)

        projects.extend(current_page_projects)

        page += 1

        if not current_page_projects:
            return projects
