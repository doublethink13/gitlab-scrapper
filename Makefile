SHELL := /bin/bash
.ONESHELL:

usage:
	@echo "Usage"
	@echo "	usage (default)"
	@echo "	make tf_init ENV='dev'"
	@echo "	make tf_apply ENV='dev'"
	@echo "	make tf_destroy ENV='dev'"
	@echo "	make tf_output ENV='dev'"
	@echo "	make ssh_into_bastion BASTION_IP='1.2.3.4'"
	@echo "	make port_forward_rds"
	@echo "	make port_forward_redshift"
	@echo "	make pgcli_rds"
	@echo "	make pgcli_redshift"
	@echo "	make freeze"
	@echo "	make install"
	@echo "	make black"
	@echo "	make isort_check"
	@echo "	make isort_apply"
	@echo "	make flake8"
	@echo "	make mypy"
	@echo "	make lint"
	@echo "	make test_one TEST=<test_name>"
	@echo "	make test_all"
	@echo "	make coverage"
	@echo "	make coverage_xml"
	@echo "	make run_local_dagit"
	@echo "	make run_container"

tf_init:
	@cd terraform/envs/$(ENV) && terraform init

tf_apply:
	@cd terraform/envs/$(ENV) && terraform apply

tf_destroy:
	@cd terraform/envs/$(ENV) && terraform destroy

tf_output:
	@cd terraform/envs/$(ENV) && terraform output

ssh_into_bastion:
	@ssh -i /workspaces/gitlab-scrapper/bastion ec2-user@$(BASTION_IP)

port_forward_rds:
	@ssh -i bastion -N rds

port_forward_redshift:
	@ssh -i bastion -N redshift

pgcli_rds:
	@pgcli -h 127.0.0.1 -p 5432 -u dagster -d dagsterdev

pgcli_redshift:
	@pgcli -h 127.0.0.1 -p 5439 -u gitlab_scrapper -d dev

freeze:
	@pip freeze > requirements.all.txt

install:
	@pip install -r requirements.dev.txt

black:
	@python -m black . --check

isort_check:
	@python -m isort . --check-only
	@python -m isort . --diff

isort_apply:
	@python -m isort .

flake8:
	@python -m flake8 .

mypy:
	@python -m mypy .

lint: black isort_check flake8 mypy

test_one:
	@python -m pytest -s -k $(TEST) -vvv

test_all:
	@python -m pytest -s

coverage:
	@python -m coverage run --source app -m pytest
	@python -m coverage report

coverage_xml: coverage
	@python -m coverage xml

run_local_dagit:
	@source .env
	@python -m dagit -f app/definitions/scrapping.py

run_container:
	@docker-compose up --build --force-recreate
