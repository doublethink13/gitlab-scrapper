import pytest
from pydantic import ValidationError

from app.models.settings import Settings
from app.types.metrics import Metrics


class TestSettings:
    def test_settings_keys(self, monkeypatch: pytest.MonkeyPatch) -> None:
        self._patch_environ(monkeypatch)

        settings = Settings()

        assert list(settings.dict().keys()) == [
            "aws_access_key_id",
            "aws_secret_access_id",
            "aws_default_region",
            "aws_bucket_name",
            "gitlab_url",
            "gitlab_private_token",
            "gitlab_oauth_token",
            "gitlab_job_token",
            "gitlab_projects_ids",
            "gitlab_owned",
            "gitlab_allowed_metrics",
            "redshift_username",
            "redshift_password",
            "redshift_engine",
            "redshift_host",
            "redshift_port",
            "redshift_db_cluster_identifier",
            "redshift_database",
        ]

    @pytest.mark.parametrize(
        [
            "env_var",
            "env_var_value",
            "expected_settings_name",
            "expected_settings_value",
        ],
        [
            ("GITLAB_PROJECTS_IDS", "123,321", "gitlab_projects_ids", {"123", "321"}),
            (
                "GITLAB_ALLOWED_METRICS",
                "pipeline",
                "gitlab_allowed_metrics",
                {Metrics.Pipeline},
            ),
            (
                "GITLAB_PRIVATE_TOKEN",
                "mockprivatetoken",
                "gitlab_private_token",
                "mockprivatetoken",
            ),
        ],
    )
    def test_correctly_parses_custom_validation(
        self,
        monkeypatch: pytest.MonkeyPatch,
        env_var: str,
        env_var_value: str,
        expected_settings_name: str,
        expected_settings_value: str,
    ) -> None:
        self._patch_environ(monkeypatch)

        monkeypatch.setenv(env_var, env_var_value)

        settings = Settings()

        assert settings.dict()[expected_settings_name] == expected_settings_value

    def _patch_environ(self, monkeypatch: pytest.MonkeyPatch) -> None:
        environ: dict[str, str | None] = {
            "AWS_ACCESS_KEY_ID": "mockaccess",
            "AWS_SECRET_ACCESS_KEY": "mocksecret",
            "AWS_DEFAULT_REGION": "mockregion",
            "AWS_BUCKET_NAME": "mockbucket",
            "GITLAB_URL": "mockgiturl",
            "REDSHIFT_USERNAME": "mockuser",
            "REDSHIFT_PASSWORD": "mockpass",
            "REDSHIFT_ENGINE": "mockengine",
            "REDSHIFT_HOST": "mockhost",
            "REDSHIFT_PORT": "123456",
            "REDSHIFT_DB_CLUSTER_IDENTIFIER": "mockidentifier",
            "REDSHIFT_DATABASE": "mockdb",
        }

        for env_var, value in environ.items():
            if value is None:
                monkeypatch.delenv(env_var, raising=False)
            else:
                monkeypatch.setenv(env_var, value)
