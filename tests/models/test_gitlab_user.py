from typing import Any

from deepdiff import DeepHash

from app.models.gitlab_user import GitLabUser

_mock_response: dict[str, Any] = {
    "name": "mockname",
    "username": "mockusername",
    "id": 1,
    "state": "mockstate",
    "avatar_url": "mockavatar",
    "web_url": "mockweburl",
}

_expected_gitlab_user = GitLabUser(
    name=_mock_response["name"],
    username=_mock_response["username"],
    id=_mock_response["id"],
    state=_mock_response["state"],
    avatar_url=_mock_response["avatar_url"],
    web_url=_mock_response["web_url"],
    hash=str(DeepHash(_mock_response)[_mock_response]),  # type:ignore
    timestamp=1,
)


class TestGitLabUser:
    def test_parse_api_response(self) -> None:
        assert GitLabUser.parse_api_response(_mock_response, 1) == _expected_gitlab_user

    def test_generate_dataframe(self) -> None:
        dataframe = GitLabUser.generate_dataframe([_expected_gitlab_user])

        assert len(dataframe.keys()) == 8

        assert dataframe["name"][0] == _expected_gitlab_user.name
        assert dataframe["username"][0] == _expected_gitlab_user.username
        assert dataframe["id"][0] == _expected_gitlab_user.id
        assert dataframe["state"][0] == _expected_gitlab_user.state
        assert dataframe["avatar_url"][0] == _expected_gitlab_user.avatar_url
        assert dataframe["web_url"][0] == _expected_gitlab_user.web_url
        assert dataframe["timestamp"][0] == _expected_gitlab_user.timestamp
        assert dataframe["hash"][0] == _expected_gitlab_user.hash
