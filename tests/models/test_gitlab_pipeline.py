from datetime import datetime
from typing import Any

import pandas
from deepdiff import DeepHash
from pandas import NaT, Timestamp

from app.models.gitlab_pipeline import GitLabPipeline
from app.types.gitlab_datetime_format import GITLAB_DATETIME_FORMAT

_mock_response: dict[str, Any] = {
    "id": 1,
    "iid": 2,
    "project_id": 3,
    "status": "status",
    "ref": "ref",
    "sha": "sha",
    "before_sha": "before_sha",
    "tag": True,
    "yaml_errors": None,
    "user": {"id": 10},
    "created_at": "1970-01-01T00:00:00.000Z",
    "updated_at": "1970-01-02T00:00:00.000Z",
    "started_at": None,
    "finished_at": "1970-01-04T00:00:00.000Z",
    "committed_at": None,
    "duration": 4.0,
    "queued_duration": 4.0,
    "coverage": "coverage",
    "web_url": "weburl",
}

_expected_gitlab_pipeline = GitLabPipeline(
    id=1,
    iid=_mock_response["iid"],
    project_id=_mock_response["project_id"],
    status=_mock_response["status"],
    ref=_mock_response["ref"],
    sha=_mock_response["sha"],
    before_sha=_mock_response["before_sha"],
    tag=_mock_response["tag"],
    yaml_errors=None,
    user=_mock_response["user"]["id"],
    created_at=datetime.strptime(_mock_response["created_at"], GITLAB_DATETIME_FORMAT),
    updated_at=datetime.strptime(_mock_response["updated_at"], GITLAB_DATETIME_FORMAT),
    started_at=None,
    finished_at=datetime.strptime(
        _mock_response["finished_at"], GITLAB_DATETIME_FORMAT
    ),
    committed_at=None,
    duration=_mock_response["duration"],
    queued_duration=_mock_response["queued_duration"],
    coverage=_mock_response["coverage"],
    web_url=_mock_response["web_url"],
    hash=str(DeepHash(_mock_response)[_mock_response]),  # type:ignore
    timestamp=1,
)


class TestGitLabPipeline:
    def test_parse_api_response(self) -> None:
        assert (
            GitLabPipeline.parse_api_response(_mock_response, 1)
            == _expected_gitlab_pipeline
        )

    def test_generate_dataframe(self) -> None:
        dataframe = GitLabPipeline.generate_dataframe([_expected_gitlab_pipeline])
        current_timezone = datetime.now().astimezone().tzinfo

        assert len(dataframe.keys()) == 21

        assert dataframe["id"][0] == _expected_gitlab_pipeline.id
        assert dataframe["iid"][0] == _expected_gitlab_pipeline.iid
        assert dataframe["project_id"][0] == _expected_gitlab_pipeline.project_id
        assert dataframe["status"][0] == _expected_gitlab_pipeline.status
        assert dataframe["ref"][0] == _expected_gitlab_pipeline.ref
        assert dataframe["sha"][0] == _expected_gitlab_pipeline.sha
        assert dataframe["before_sha"][0] == _expected_gitlab_pipeline.before_sha
        assert pandas.isnull(dataframe["yaml_errors"][0])
        assert dataframe["user"][0] == _expected_gitlab_pipeline.user
        assert dataframe["created_at"][0] == Timestamp(
            ts_input=_expected_gitlab_pipeline.created_at, tz=current_timezone
        )
        assert dataframe["updated_at"][0] == Timestamp(
            ts_input=_expected_gitlab_pipeline.updated_at, tz=current_timezone
        )
        assert pandas.isnull(dataframe["started_at"][0])
        assert dataframe["finished_at"][0] == Timestamp(
            ts_input=_expected_gitlab_pipeline.finished_at, tz=current_timezone
        )
        assert pandas.isnull(dataframe["committed_at"][0])
        assert dataframe["duration"][0] == _expected_gitlab_pipeline.duration
        assert (
            dataframe["queued_duration"][0] == _expected_gitlab_pipeline.queued_duration
        )
        assert dataframe["coverage"][0] == _expected_gitlab_pipeline.coverage
        assert dataframe["web_url"][0] == _expected_gitlab_pipeline.web_url
        assert dataframe["timestamp"][0] == _expected_gitlab_pipeline.timestamp
        assert dataframe["hash"][0] == _expected_gitlab_pipeline.hash
