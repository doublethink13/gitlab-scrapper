from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.resources.gitlab_api import GitLabApi
from tests.mocks.gitlab import MockGitLab
from tests.mocks.settings import MockSettings


class TestGitLabApi:
    patch_path_prefix = "app.resources.gitlab_api"

    @pytest.fixture(autouse=True)
    def mock_settings_instance(self) -> MockSettings:
        return MockSettings()

    @pytest.fixture(autouse=True)
    def mock_settings(
        self, mock_settings_instance: MockSettings
    ) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.Settings") as mock:
            mock.return_value = mock_settings_instance

            yield mock

    @pytest.fixture(autouse=True)
    def mock_gitlab_instance(self) -> MockGitLab:
        return MockGitLab()

    @pytest.fixture(autouse=True)
    def mock_gitlab(
        self, mock_gitlab_instance: MockGitLab
    ) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.Gitlab") as mock:
            mock.return_value = mock_gitlab_instance

            yield mock

    @pytest.fixture(autouse=True)
    def mock_get_all_gitlab_resources(self) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.get_all_gitlab_resources") as mock:
            mock.return_value = []

            yield mock

    def test_init(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_settings_instance: MockSettings,
    ) -> None:
        mock_load_api = Mock()
        monkeypatch.setattr(GitLabApi, "_load_api", mock_load_api)

        mock_load_projects = Mock()
        monkeypatch.setattr(GitLabApi, "_load_projects", mock_load_projects)

        self._create_gitlab_api(mock_settings_instance)

        mock_load_api.assert_called_once_with()
        mock_load_projects.assert_called_once_with()

    def test_init_loads_api(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_settings_instance: MockSettings,
        mock_gitlab: Mock,
        mock_gitlab_instance: MockGitLab,
    ) -> None:
        mock_load_projects = Mock()
        monkeypatch.setattr(GitLabApi, "_load_projects", mock_load_projects)

        self._create_gitlab_api(mock_settings_instance)

        mock_gitlab.assert_called_once_with(
            mock_settings_instance.gitlab_url,
            private_token=mock_settings_instance.gitlab_private_token,
            oauth_token=mock_settings_instance.gitlab_oauth_token,
            job_token=mock_settings_instance.gitlab_job_token,
            order_by="id",
            per_page=20,
        )

        mock_gitlab_instance.auth.assert_called_once_with()

    def test_init_loads_specific_project(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_settings_instance: MockSettings,
        mock_gitlab_instance: MockGitLab,
    ) -> None:
        monkeypatch.setattr(
            GitLabApi, "_load_api", Mock(return_value=mock_gitlab_instance)
        )

        project_id = "mockprojectid"
        mock_settings_instance.gitlab_projects_ids = {project_id}

        gitlab = self._create_gitlab_api(mock_settings_instance)

        mock_gitlab_instance.projects.get.assert_called_once_with(project_id)

        assert gitlab.projects == [mock_gitlab_instance.projects.return_value]

    def test_init_loads_all_projects(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_settings_instance: MockSettings,
        mock_gitlab_instance: MockGitLab,
        mock_get_all_gitlab_resources: Mock,
    ) -> None:
        monkeypatch.setattr(
            GitLabApi, "_load_api", Mock(return_value=mock_gitlab_instance)
        )

        gitlab = self._create_gitlab_api(mock_settings_instance)

        mock_get_all_gitlab_resources.assert_called_once_with(
            gitlab.api.projects,
            owned=mock_settings_instance.gitlab_owned,
        )

        assert gitlab.projects == mock_get_all_gitlab_resources.return_value

    def _create_gitlab_api(self, mock_settings_instance: MockSettings) -> GitLabApi:
        return GitLabApi(
            gitlab_url=mock_settings_instance.gitlab_url,
            gitlab_private_token=mock_settings_instance.gitlab_private_token,
            gitlab_oauth_token=mock_settings_instance.gitlab_oauth_token,
            gitlab_job_token=mock_settings_instance.gitlab_job_token,
            gitlab_projects_ids=mock_settings_instance.gitlab_projects_ids,
            gitlab_owned=mock_settings_instance.gitlab_owned,
            gitlab_allowed_metrics=mock_settings_instance.gitlab_allowed_metrics,
        )
