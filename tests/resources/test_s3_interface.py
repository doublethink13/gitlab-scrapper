from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.resources.s3_api import S3Api
from tests.mocks.boto3 import MockBoto3
from tests.mocks.settings import MockSettings


class TestS3Api:
    patch_path_prefix = "app.resources.s3_api"

    @pytest.fixture(autouse=True)
    def mock_settings_instance(self) -> MockSettings:
        return MockSettings()

    @pytest.fixture(autouse=True)
    def mock_settings(
        self, mock_settings_instance: MockSettings
    ) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.Settings") as mock:
            mock.return_value = mock_settings_instance

            yield mock

    @pytest.fixture(autouse=True)
    def mock_boto3(self) -> MockBoto3:
        return MockBoto3()

    @pytest.fixture(autouse=True)
    def mock_session(self, mock_boto3: Mock) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.Session") as mock:
            mock.return_value = mock_boto3

            yield mock

    def test_init(
        self, mock_settings_instance: MockSettings, mock_boto3: MockBoto3
    ) -> None:
        self._generate_s3_api(mock_settings_instance)

        mock_boto3.resource.assert_called_once_with("s3")

        mock_boto3.Bucket.assert_called_once_with(
            mock_settings_instance.aws_bucket_name
        )

    def test_bucket(
        self, mock_settings_instance: MockSettings, mock_boto3: MockBoto3
    ) -> None:
        s3_api = self._generate_s3_api(mock_settings_instance)

        assert s3_api.bucket == mock_boto3.Bucket.return_value

    def test_upload_file(
        self, mock_settings_instance: MockSettings, mock_boto3: MockBoto3
    ) -> None:
        s3_api = self._generate_s3_api(mock_settings_instance)

        filename = "mockfilename"
        object_key = "mockkey"

        file_uri = s3_api.upload_file(filename, object_key)

        mock_boto3.bucket.upload_file.assert_called_once_with(filename, object_key)

        assert file_uri == f"s3://{mock_boto3.bucket.name}/{object_key}"

    def test_load_bucket_keys(
        self, mock_settings_instance: MockSettings, mock_boto3: MockBoto3
    ) -> None:
        s3_api = self._generate_s3_api(mock_settings_instance)

        keys = s3_api.load_bucket_keys()

        assert keys == [mock_boto3.bucket.objects.objects[0].key]

    def _generate_s3_api(self, mock_settings_instance: MockSettings) -> S3Api:
        return S3Api(aws_bucket_name=mock_settings_instance.aws_bucket_name)
