from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.resources.database import Database
from tests.mocks.engine import MockEngine
from tests.mocks.session import MockSession
from tests.mocks.settings import MockSettings
from tests.mocks.table import MockTable


class TestDatabase:
    patch_path_prefix = "app.resources.database"

    @pytest.fixture(autouse=True)
    def mock_settings_instance(self) -> MockSettings:
        return MockSettings()

    @pytest.fixture(autouse=True)
    def mock_settings(
        self, mock_settings_instance: MockSettings
    ) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.Settings") as mock:
            mock.return_value = mock_settings_instance
            yield mock

    @pytest.fixture(autouse=True)
    def mock_session_instance(self) -> MockSession:
        return MockSession()

    @pytest.fixture(autouse=True)
    def mock_session(
        self, mock_session_instance: MockSession
    ) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.Session") as mock:
            mock.return_value = mock_session_instance
            yield mock

    @pytest.fixture(autouse=True)
    def mock_engine(self) -> MockEngine:
        return MockEngine()

    @pytest.fixture(autouse=True)
    def mock_table(self) -> MockTable:
        return MockTable()

    @pytest.fixture(autouse=True)
    def mock_create_engine(self) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.create_engine") as mock:
            yield mock

    @pytest.fixture(autouse=True)
    def mock_url_create(self) -> Generator[Mock, None, None]:
        with patch(f"{self.patch_path_prefix}.URL.create") as mock:
            mock.return_value = "mockurl"

            yield mock

    def test_init(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_settings: Mock,
        mock_engine: MockEngine,
    ) -> None:
        _, mock_create_engine = self._generate_database(monkeypatch, mock_engine)

        mock_settings.assert_called_once_with()

        mock_create_engine.assert_called_once_with()

    def test_session(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_session: Mock,
        mock_session_instance: MockSession,
        mock_engine: MockEngine,
    ) -> None:
        database, _ = self._generate_database(monkeypatch, mock_engine)

        with database.session() as _:
            ...

        mock_session.assert_called_once_with(bind=mock_engine)

        mock_session_instance.begin.assert_called_once_with()

    def test_connect(
        self, monkeypatch: pytest.MonkeyPatch, mock_engine: MockEngine
    ) -> None:
        database, _ = self._generate_database(monkeypatch, mock_engine)

        with database.connection() as _:
            ...

        mock_engine.connect.assert_called_once_with()

    def test_ensure_table(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_engine: MockEngine,
        mock_table: MockTable,
    ) -> None:
        database, _ = self._generate_database(monkeypatch, mock_engine)

        database.ensure_table(mock_table)

        mock_table.create.assert_called_once_with(bind=mock_engine, checkfirst=True)

    def test_create_engine(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_settings_instance: MockSettings,
        mock_url_create: Mock,
        mock_create_engine: Mock,
    ) -> None:
        self._generate_database(monkeypatch)

        mock_url_create.assert_called_once_with(
            drivername=f"{mock_settings_instance.redshift_engine}+psycopg2",
            username=f"{mock_settings_instance.redshift_username}",
            password=f"{mock_settings_instance.redshift_password}",
            host=f"{mock_settings_instance.redshift_host}",
            port=mock_settings_instance.redshift_port,
            database=f"{mock_settings_instance.redshift_database}",
        )

        mock_create_engine.assert_called_once_with(
            mock_url_create.return_value,
            connect_args={"connect_timeout": 60 * 60 * 16, "sslmode": "verify-ca"},
        )

    def _generate_database(
        self, monkeypatch: pytest.MonkeyPatch, mock_engine: MockEngine = None
    ) -> tuple[Database, Mock]:
        mock_create_engine = Mock(return_value=mock_engine)
        if mock_engine:
            monkeypatch.setattr(Database, "_create_engine", mock_create_engine)

        return Database(), mock_create_engine
