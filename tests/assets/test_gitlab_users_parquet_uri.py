from typing import Generator
from unittest.mock import Mock, patch

import pytest
from pandas import DataFrame

from app.assets.gitlab_users_parquet_uri import gitlab_users_parquet_uri
from tests.mocks.s3_api import MockS3Api


class TestGitlabUsersParquetUri:
    @pytest.fixture(autouse=True)
    def timestamp(self) -> int:
        return 1

    @pytest.fixture(autouse=True)
    def mock_dataframe(self) -> DataFrame:
        return DataFrame()

    @pytest.fixture(autouse=True)
    def mock_s3_api(self) -> MockS3Api:
        return MockS3Api()

    @pytest.fixture(autouse=True)
    def mock_upload_dataframe_as_parquet_to_s3(self) -> Generator[Mock, None, None]:
        with patch(
            "app.assets.gitlab_users_parquet_uri.upload_dataframe_as_parquet_to_s3"
        ) as mock:
            mock.return_value = "mockstring"

            yield mock

    def test_gitlab_users_parquet_uri(
        self,
        timestamp: int,
        mock_dataframe: DataFrame,
        mock_s3_api: MockS3Api,
        mock_upload_dataframe_as_parquet_to_s3: Mock,
    ) -> None:
        gitlab_users_parquet_uri(
            timestamp=timestamp,
            gitlab_users_dataframe=mock_dataframe,
            s3=mock_s3_api,
        )

        mock_upload_dataframe_as_parquet_to_s3.assert_called_once_with(
            bucket_name=mock_s3_api.bucket.name,
            timestamp=timestamp,
            model="users",
            dataframe=mock_dataframe,
        )
