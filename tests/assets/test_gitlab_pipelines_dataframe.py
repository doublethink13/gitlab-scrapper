from typing import Any, Generator
from unittest.mock import ANY, Mock, patch

import pytest
from dagster import OpExecutionContext, build_op_context
from pandas import DataFrame

from app.assets.gitlab_pipelines_dataframe import gitlab_pipelines_dataframe
from app.models.gitlab_pipeline import GitLabPipeline


class TestGitlabPipelinesDataframe:
    @pytest.fixture(autouse=True)
    def mock_generate_dataframe(self) -> Generator[Mock, None, None]:
        with patch("app.assets.gitlab_pipelines_dataframe.generate_dataframe") as mock:
            mock.return_value = DataFrame()

            yield mock

    @pytest.fixture(autouse=True)
    def context(self) -> OpExecutionContext:
        return build_op_context()

    @pytest.fixture(autouse=True)
    def timestamp(self) -> int:
        return 1

    @pytest.fixture(autouse=True)
    def gitlab_pipelines_asdicts(self) -> list[dict[str, Any]]:
        return [{}]

    def test_gitlab_pipelines_dataframe(
        self,
        mock_generate_dataframe: Mock,
        context: OpExecutionContext,
        timestamp: int,
        gitlab_pipelines_asdicts: dict[str, Any],
    ) -> None:
        gitlab_pipelines_dataframe(
            context=context,
            timestamp=timestamp,
            gitlab_pipelines_asdict=gitlab_pipelines_asdicts,
        )

        mock_generate_dataframe.assert_called_once_with(
            context=ANY,
            model=GitLabPipeline,
            timestamp=timestamp,
            asdicts=gitlab_pipelines_asdicts,
        )
