from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.assets.gitlab_pipelines_table import gitlab_pipelines_table
from app.redshift_tables.gitlab_pipelines import GitLabPipelines
from tests.mocks.database import MockDatabase


class TestGitlabPipelinesTable:
    @pytest.fixture(autouse=True)
    def mock_update_redshift_table(self) -> Generator[Mock, None, None]:
        with patch("app.assets.gitlab_pipelines_table.update_redshift_table") as mock:
            yield mock

    def test_gitlab_pipelines_table(self, mock_update_redshift_table: Mock) -> None:
        gitlab_pipelines_parquet_uri = "mockuri"
        database = MockDatabase()

        gitlab_pipelines_table(
            gitlab_pipelines_parquet_uri=gitlab_pipelines_parquet_uri, database=database
        )

        mock_update_redshift_table.assert_called_once_with(
            gitlab_pipelines_parquet_uri, database, GitLabPipelines
        )
