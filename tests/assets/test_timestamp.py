from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.assets.timestamp import timestamp
from tests.mocks.datetime import MockDatetime


class TestTimestamp:
    @pytest.fixture(autouse=True)
    def mock_datetime(self) -> MockDatetime:
        return MockDatetime()

    @pytest.fixture(autouse=True)
    def mock_now(self, mock_datetime: MockDatetime) -> Generator[Mock, None, None]:
        with patch("app.assets.timestamp.datetime", mock_datetime) as mock:
            mock.return_value = mock_datetime

            yield mock

    def test_timestamp(self, mock_datetime: MockDatetime) -> None:
        timestamp()

        mock_datetime.now.assert_called_once_with()
        mock_datetime.timestamp.assert_called_once_with()
