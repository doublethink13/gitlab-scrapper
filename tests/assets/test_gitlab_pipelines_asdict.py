from typing import Generator
from unittest.mock import Mock, patch

import pytest
from dagster import build_op_context

from app.assets.gitlab_pipelines_asdict import gitlab_pipelines_asdict
from tests.mocks.gitlab_api import MockGitLabApi
from tests.mocks.gitlab_pipeline import MockGitLabPipeline
from tests.mocks.gitlab_simple_gitlab_pipeline import MockSimpleGitLabPipeline


class TestGitlabPipelinesAsdict:
    @pytest.fixture(autouse=True)
    def mock_gitlab_api(self) -> MockGitLabApi:
        return MockGitLabApi()

    @pytest.fixture(autouse=True)
    def mock_simple_pipeline(self) -> MockSimpleGitLabPipeline:
        return MockSimpleGitLabPipeline()

    @pytest.fixture(autouse=True)
    def mock_get_all_gitlab_resources(
        self, mock_simple_pipeline: MockSimpleGitLabPipeline
    ) -> Generator[Mock, None, None]:
        with patch(
            "app.assets.gitlab_pipelines_asdict.get_all_gitlab_resources"
        ) as mock:
            mock.return_value = [mock_simple_pipeline]

            yield mock

    def test_gitlab_pipelines_asdict(
        self,
        mock_gitlab_api: MockGitLabApi,
        mock_get_all_gitlab_resources: Mock,
        mock_simple_pipeline: MockSimpleGitLabPipeline,
    ) -> None:
        gitlab_pipelines = gitlab_pipelines_asdict(
            context=build_op_context(), gitlab=mock_gitlab_api
        )

        mock_get_all_gitlab_resources.assert_called_once_with(
            mock_gitlab_api.projects[0].pipelines
        )

        mock_simple_pipeline.get_id.assert_called_once_with()

        mock_gitlab_api.projects[0].pipelines.get.assert_called_once_with(
            mock_simple_pipeline.get_id.return_value
        )

        mock_gitlab_pipeline: MockGitLabPipeline = mock_gitlab_api.projects[
            0
        ].pipelines.get.return_value
        assert gitlab_pipelines == [mock_gitlab_pipeline.asdict.return_value]
