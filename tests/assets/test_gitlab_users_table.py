from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.assets.gitlab_users_table import gitlab_users_table
from app.redshift_tables.gitlab_users import GitLabUsers
from tests.mocks.database import MockDatabase


class TestGitlabUsersTable:
    @pytest.fixture(autouse=True)
    def mock_update_redshift_table(self) -> Generator[Mock, None, None]:
        with patch("app.assets.gitlab_users_table.update_redshift_table") as mock:
            yield mock

    def test_gitlab_users_table(self, mock_update_redshift_table: Mock) -> None:
        gitlab_users_parquet_uri = "mockuri"
        database = MockDatabase()

        gitlab_users_table(
            gitlab_users_parquet_uri=gitlab_users_parquet_uri, database=database
        )

        mock_update_redshift_table.assert_called_once_with(
            gitlab_users_parquet_uri, database, GitLabUsers
        )
