from typing import Any

import pytest
from dagster import build_op_context

from app.utils.generate_dataframe import generate_dataframe
from tests.mocks.gitlab_model import MockGitLabModel


class TestGenerateDataframe:
    @pytest.fixture(autouse=True)
    def mock_gitlab_model(self) -> MockGitLabModel:
        return MockGitLabModel()

    @pytest.fixture(autouse=True)
    def timestamp(self) -> int:
        return 1

    @pytest.fixture(autouse=True)
    def gitlab_pipelines_asdict(self) -> dict[str, Any]:
        return {}

    def test_generate_dataframe(
        self,
        mock_gitlab_model: MockGitLabModel,
        timestamp: int,
        gitlab_pipelines_asdict: dict[str, Any],
    ) -> None:
        dataframe = generate_dataframe(
            context=build_op_context(),
            model=mock_gitlab_model,
            timestamp=timestamp,
            asdicts=[gitlab_pipelines_asdict],
        )

        mock_gitlab_model.parse_api_response.assert_called_once_with(
            gitlab_pipelines_asdict, timestamp
        )

        mock_gitlab_model.generate_dataframe.assert_called_once_with(
            [mock_gitlab_model.parse_api_response.return_value]
        )

        assert dataframe == mock_gitlab_model.generate_dataframe.return_value
