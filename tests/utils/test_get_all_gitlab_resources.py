import pytest

from app.utils.get_all_gitlab_resources import get_all_gitlab_resources
from tests.mocks.project_manager import MockProjectManager
from tests.mocks.project_pipeline_manager import MockProjectPipelineManager


class TestGetAllGitlabResources:
    @pytest.fixture(autouse=True)
    def mock_project_manager(self) -> MockProjectManager:
        return MockProjectManager()

    @pytest.fixture(autouse=True)
    def mock_project_pipeline_manager(self) -> MockProjectPipelineManager:
        return MockProjectPipelineManager()

    def test_get_all_gitlab_resources_project_manager(
        self, mock_project_manager: MockProjectManager
    ) -> None:
        args = {"owned": True}

        get_all_gitlab_resources(mock_project_manager, **args)

        mock_project_manager.list.assert_called_once_with(page=1, **args)

    def test_get_all_gitlab_resources_project_pipeline_manager(
        self, mock_project_pipeline_manager: MockProjectPipelineManager
    ) -> None:
        args = {"owned": True}

        get_all_gitlab_resources(mock_project_pipeline_manager, **args)

        mock_project_pipeline_manager.list.assert_called_once_with(page=1, **args)
