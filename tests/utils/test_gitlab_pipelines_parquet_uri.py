from typing import Generator
from unittest.mock import Mock, patch

import pytest
from pandas import DataFrame

from app.utils.upload_dataframe_as_parquet_to_s3 import (
    upload_dataframe_as_parquet_to_s3,
)
from tests.mocks.s3_api import MockS3Api


class TestUploadDataframeAsParquetToS3:
    @pytest.fixture(autouse=True)
    def model(self) -> str:
        return "mockmodel"

    @pytest.fixture(autouse=True)
    def timestamp(self) -> int:
        return 1

    @pytest.fixture(autouse=True)
    def mock_dataframe(self) -> DataFrame:
        return DataFrame()

    @pytest.fixture(autouse=True)
    def mock_s3_api(self) -> MockS3Api:
        return MockS3Api()

    @pytest.fixture(autouse=True)
    def mock_to_parquet(self) -> Generator[Mock, None, None]:
        with patch(
            "app.utils.upload_dataframe_as_parquet_to_s3.wr.s3.to_parquet"
        ) as mock:
            yield mock

    def test_upload_dataframe_as_parquet_to_s3(
        self,
        model: str,
        timestamp: int,
        mock_dataframe: DataFrame,
        mock_s3_api: MockS3Api,
        mock_to_parquet: Mock,
    ) -> None:
        expected_uri = f"s3://{mock_s3_api.bucket.name}/{timestamp}/{model}.parquet"

        uri = upload_dataframe_as_parquet_to_s3(
            bucket_name=mock_s3_api.bucket.name,
            timestamp=timestamp,
            model=model,
            dataframe=mock_dataframe,
        )

        mock_to_parquet.assert_called_once_with(mock_dataframe, path=expected_uri)

        assert uri == expected_uri
