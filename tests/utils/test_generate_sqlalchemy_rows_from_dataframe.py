from copy import deepcopy

from numpy import nan
from pandas import NaT

from app.models.gitlab_pipeline import GitLabPipeline
from app.models.gitlab_user import GitLabUser
from app.redshift_tables.gitlab_pipelines import GitLabPipelines
from app.redshift_tables.gitlab_users import GitLabUsers
from app.utils.generate_sqlalchemy_rows_from_dataframe import (
    generate_sqlalchemy_rows_from_dataframe,
)


class TestGenerateSqlalchemyRowsFromDataframe:
    def test_generate_sqlalchemy_rows_from_dataframe_gitlab_users(self) -> None:
        _hash = "hash"

        column_values = {
            "id": 1,
            "name": "name",
            "username": "username",
            "state": "state",
            "avatar_url": "avatarurl",
            "web_url": "weburl",
            "hash": _hash,
            "timestamp": 1,
        }

        df = GitLabUser.generate_dataframe([GitLabUser(**column_values)])

        rows, hashes = generate_sqlalchemy_rows_from_dataframe(df, GitLabUsers)

        self._assert_gitlab_user_row(rows[0], GitLabUsers(**column_values))

        assert hashes == [_hash]

    def test_generate_sqlalchemy_rows_from_dataframe_gitlab_pipelines(self) -> None:
        _hash = "hash"

        column_values = {
            "id": 1,
            "iid": 2,
            "project_id": 3,
            "status": "status",
            "ref": "ref",
            "sha": "sha",
            "before_sha": "before_sha",
            "tag": True,
            "yaml_errors": None,
            "user": 10,
            "created_at": "1970-01-01T00:00:00.000Z",
            "updated_at": "1970-01-02T00:00:00.000Z",
            "started_at": None,
            "finished_at": "1970-01-04T00:00:00.000Z",
            "committed_at": None,
            "duration": 4.0,
            "queued_duration": 4.0,
            "coverage": "coverage",
            "web_url": "weburl",
            "hash": _hash,
            "timestamp": 1,
        }

        df = GitLabPipeline.generate_dataframe([GitLabPipeline(**column_values)])

        column_values_to_create_row = deepcopy(column_values)
        df_copy = deepcopy(df)
        df_copy.replace({nan: None, NaT: None}, inplace=True)
        column_values_to_create_row["created_at"] = df_copy["created_at"][0]
        column_values_to_create_row["updated_at"] = df_copy["updated_at"][0]
        column_values_to_create_row["started_at"] = df_copy["started_at"][0]
        column_values_to_create_row["finished_at"] = df_copy["finished_at"][0]
        column_values_to_create_row["committed_at"] = df_copy["committed_at"][0]

        rows, hashes = generate_sqlalchemy_rows_from_dataframe(df, GitLabPipelines)

        self._assert_gitlab_pipeline_row(
            rows[0],
            GitLabPipelines(
                **column_values_to_create_row,
            ),
        )

        assert hashes == [_hash]

    def _assert_gitlab_user_row(
        self, tested_row: GitLabUsers, expected_row: GitLabUsers
    ) -> None:
        assert tested_row.id == expected_row.id
        assert tested_row.name == expected_row.name
        assert tested_row.username == expected_row.username
        assert tested_row.state == expected_row.state
        assert tested_row.avatar_url == expected_row.avatar_url
        assert tested_row.web_url == expected_row.web_url
        assert tested_row.hash == expected_row.hash
        assert tested_row.timestamp == expected_row.timestamp

    def _assert_gitlab_pipeline_row(
        self, tested_row: GitLabPipelines, expected_row: GitLabPipelines
    ) -> None:
        assert tested_row.id == expected_row.id
        assert tested_row.iid == expected_row.iid
        assert tested_row.project_id == expected_row.project_id
        assert tested_row.status == expected_row.status
        assert tested_row.ref == expected_row.ref
        assert tested_row.sha == expected_row.sha
        assert tested_row.before_sha == expected_row.before_sha
        assert tested_row.tag == expected_row.tag
        assert tested_row.yaml_errors == expected_row.yaml_errors
        assert tested_row.user == expected_row.user
        assert tested_row.created_at == expected_row.created_at
        assert tested_row.updated_at == expected_row.updated_at
        assert tested_row.started_at == expected_row.started_at
        assert tested_row.finished_at == expected_row.finished_at
        assert tested_row.committed_at == expected_row.committed_at
        assert tested_row.duration == expected_row.duration
        assert tested_row.queued_duration == expected_row.queued_duration
        assert tested_row.coverage == expected_row.coverage
        assert tested_row.hash == expected_row.hash
        assert tested_row.timestamp == expected_row.timestamp
