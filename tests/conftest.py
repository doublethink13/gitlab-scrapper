import pytest

from app.models.settings import Settings


@pytest.fixture(autouse=True, scope="function")
def patch_env(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setattr(Settings.Config, "env_file", "")
