from unittest.mock import Mock

from tests.mocks.projects_manager import MockProjectsManager


class MockGitLab:
    def __init__(
        self,
    ) -> None:
        self.url = "url"
        self.private_token = "private_token"
        self.oauth_token = "oauth_token"
        self.job_token = "job_token"
        self.order_by = "order_by"
        self.per_page = 1

        self.auth = Mock()

        self.projects = MockProjectsManager()
