from tests.mocks.project_pipeline_manager import MockProjectPipelineManager


class MockGitLabProject:
    def __init__(self) -> None:
        self.pipelines = MockProjectPipelineManager()
