from unittest.mock import Mock

from tests.mocks.object import MockObject


class MockObjects:
    def __init__(self) -> None:
        self.objects = [MockObject()]
        self.all = Mock(return_value=self.objects)
