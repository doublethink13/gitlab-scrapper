from tests.mocks.gitlab_project import MockGitLabProject


class MockGitLabApi:
    def __init__(self) -> None:
        self.projects = [MockGitLabProject()]
