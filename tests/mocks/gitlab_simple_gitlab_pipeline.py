from unittest.mock import Mock


class MockSimpleGitLabPipeline:
    def __init__(self) -> None:
        self.get_id = Mock(return_value=1)
