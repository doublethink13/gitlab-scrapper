from tests.mocks.bucket import MockBucket


class MockS3Api:
    def __init__(self) -> None:
        self.bucket = MockBucket()
