from typing import Any
from unittest.mock import Mock


class MockSession:
    def __init__(self) -> None:
        self.begin = Mock(return_value=self)

    def __enter__(self) -> "MockSession":
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        ...
