from unittest.mock import Mock

from tests.mocks.objects import MockObjects


class MockBucket:
    def __init__(self) -> None:
        self.name = "mockname"
        self.upload_file = Mock()
        self.objects = MockObjects()
