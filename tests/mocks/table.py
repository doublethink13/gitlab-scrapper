from unittest.mock import Mock


class MockTable:
    def __init__(self) -> None:
        self.create = Mock()

    @property
    def __table__(self) -> "MockTable":
        return self
