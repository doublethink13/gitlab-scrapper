from unittest.mock import Mock


class MockDatetime:
    def __init__(self) -> None:
        self.now = Mock(return_value=self)
        self.timestamp = Mock(return_value=1)
