from typing import Any

from app.types.metrics import Metrics


class MockSettings:
    def __init__(self) -> None:
        self.aws_access_key_id = "aws_access_key_id"
        self.aws_secret_access_id = "aws_secret_access_id"
        self.aws_default_region = "aws_default_region"
        self.aws_bucket_name = "aws_bucket_name"

        self.gitlab_url = "gitlab_url"
        self.gitlab_private_token = "gitlab_private_token"
        self.gitlab_oauth_token = "gitlab_oauth_token"
        self.gitlab_job_token = "gitlab_job_token"
        self.gitlab_projects_ids: set[str] = set[str]()
        self.gitlab_owned: bool | None = None
        self.gitlab_allowed_metrics: set[Metrics] = set[Metrics]()

        self.redshift_username = "redshift_username"
        self.redshift_password = "redshift_password"
        self.redshift_engine = "redshift_engine"
        self.redshift_host = "redshift+host"
        self.redshift_port = 123456
        self.redshift_db_cluster_identifier = "redshift_identifier"
        self.redshift_database = "redshift_db"

    def __getitem__(self, key: str) -> Any:
        return getattr(self, key)

    def dict(self) -> None:
        ...
