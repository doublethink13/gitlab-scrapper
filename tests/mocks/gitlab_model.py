from unittest.mock import Mock


class MockGitLabModel:
    def __init__(self) -> None:
        self.parse_api_response = Mock()
        self.generate_dataframe = Mock()
