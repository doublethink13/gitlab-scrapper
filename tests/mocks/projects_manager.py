from unittest.mock import Mock


class MockProjectsManager:
    def __init__(self) -> None:
        self.return_value = "mockget"
        self.get = Mock(return_value=self.return_value)
