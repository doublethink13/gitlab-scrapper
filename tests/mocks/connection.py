from typing import Any


class MockConnection:
    def __init__(self) -> None:
        ...

    def __enter__(self) -> "MockConnection":
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        ...
