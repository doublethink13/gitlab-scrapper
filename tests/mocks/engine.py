from unittest.mock import Mock

from tests.mocks.connection import MockConnection


class MockEngine:
    def __init__(self) -> None:
        self.connection = MockConnection()
        self.connect = Mock(return_value=self.connection)
