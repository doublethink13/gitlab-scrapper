from unittest.mock import Mock


class MockGitLabPipeline:
    def __init__(self) -> None:
        self.asdict = Mock(return_value={"hello": "world"})
