from typing import Any
from unittest.mock import Mock

from tests.mocks.gitlab_pipeline import MockGitLabPipeline


class MockProjectPipelineManager(Mock):
    def __init__(
        self,
        spec: Any | None = None,
        side_effect: Any | None = None,
        return_value: Any = ...,
        wraps: Any | None = None,
        name: Any | None = None,
        spec_set: Any | None = None,
        parent: Any | None = None,
        _spec_state: Any | None = None,
        _new_name: Any = "",
        _new_parent: Any | None = None,
        **kwargs: Any
    ) -> None:
        super().__init__(
            spec,
            side_effect,
            return_value,
            wraps,
            name,
            spec_set,
            parent,
            _spec_state,
            _new_name,
            _new_parent,
            **kwargs
        )

        self.get = Mock(return_value=MockGitLabPipeline())
        self.list = Mock(return_value=[])
