from unittest.mock import Mock

from tests.mocks.bucket import MockBucket


class MockBoto3:
    def __init__(self) -> None:
        self.bucket = MockBucket()
        self.resource = Mock(return_value=self)
        self.Bucket = Mock(return_value=self.bucket)
