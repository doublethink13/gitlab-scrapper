# GitLab Scrapper

## Installation

Download a container from [https://gitlab.com/doublethink13/gitlab-scrapper/container_registry/4036599](https://gitlab.com/doublethink13/gitlab-scrapper/container_registry/4036599).

To run the container, make sure the following env vars are correctly set (there is a per job configuration below):

```bash
DAGSTER_RDS_USERNAME="dagster"
DAGSTER_RDS_PASSWORD="Pass11032023"
DAGSTER_RDS_HOST="127.0.0.1"
DAGSTER_RDS_PORT="5432"
DAGSTER_RDS_DB="dagsterdev"

AWS_ACCESS_KEY_ID=""
AWS_SECRET_ACCESS_KEY=""
AWS_DEFAULT_REGION="eu-central-1"
AWS_BUCKET_NAME="gitlab-scrapper-raw-data-dev"

GITLAB_URL="https://gitlab.com/"
GITLAB_PRIVATE_TOKEN=""
# GITLAB_OAUTH_TOKEN=""
# GITLAB_JOB_TOKEN=""
GITLAB_PROJECTS_IDS="33658220"
GITLAB_OWNED="true"
GITLAB_ALLOWED_METRICS="pipeline"

REDSHIFT_USERNAME="dagster"
REDSHIFT_PASSWORD="Pass11032023"
REDSHIFT_ENGINE="redshift"
REDSHIFT_HOST="127.0.0.1"
REDSHIFT_PORT="5439"
REDSHIFT_DB_CLUSTER_IDENTIFIER="mock"
REDSHIFT_DATABASE="dev"
```

Dagit is accessed from the 3000 port.

## Local development

Open a devcontainer from VSCode. Everything in the docs assumes development is made from inside a devcontainer. Using a virtualenv should be, for the most part, similar.

Local development is also done connecting to AWS resources via a bastion instance. Check Terraform folder for additional information.

## Running dagit locally

Create a copy of `.env.example` named `.env` and update the necessary values. Every env var needed to run `dagit` locally are in the `.env.example`. This is also true to start a container.

Note that the necessary AWS resources can be created with this repo using Terraform. Check the [Terraform](#terraform) section for additional information.

Have a look at the [RDS](#rds) and [Redshift](#redshift) section to setup the necessary connections.

Finally, when everything is setup, run:

```bash
make run_local_dagit
```

### Terraform

Make sure Terraform is installed.

To create the necessary resources in a specific env, run:

```bash
make tf_init ENV="dev"
make tf_apply ENV="dev"
```

To destroy the env, run:

```bash
make tf_destroy ENV="dev"
```

### RDS

Note that some values, like the Bastion IP, will change in the future. The ones in this guide are an example. The devcontainer exposes the `5432` port to connect to RDS.

Forward you local port to RDS using the Bastion instance. First, update the `~/.ssh/config` using the output values from `make tf_output ENV='dev'`:

```txt
Host rds
  User ec2-user
  IdentityFile ~/.ssh/bastion
  HostName 3.67.208.0
  LocalForward 5432 terraform-20230504143342170800000001.ck9o4if8pqoe.eu-central-1.rds.amazonaws.com:5432
  ForwardAgent yes
  TCPKeepAlive yes
  ConnectTimeout 5
  ServerAliveCountMax 10
  ServerAliveInterval 15
```

If you are not using this repo's Terraform resources, you may update the `config` file to suit your needs. If you are using this repo's `make`, please name the `Host` `rds`

Then, run from a separate shell:

```bash
make port_forward_rds
```

It's possible to confirm that everything is working with `pgcli`:

```bash
pgcli -h 127.0.0.1 -p 5432 -u dagster -d dev
```

#### Troubleshoot

If RDS is not reachable, try reach it from the bastion:

```bash
sudo yum install telnet
telnet hostname port
```

### Redshift

Note that some values, like the bastion IP, will change in the future. The ones in this guide are an example. The devcontainer exposes the `5439` port to connect to Redshift.

Forward you local port to Redshift using the bastion instance. First, update the `~/.ssh/config` using the output values from `make tf_output ENV='dev'`.

```txt
Host redshift
  User ec2-user
  IdentityFile ~/.ssh/bastion
  HostName 3.67.208.0
  LocalForward 5439 gitlab-scrapper-dev.cryp5chmrudq.eu-central-1.redshift.amazonaws.com:5439
  ForwardAgent yes
  TCPKeepAlive yes
  ConnectTimeout 5
  ServerAliveCountMax 10
  ServerAliveInterval 15
```

If you are not using this repo's Terraform resources, you may update the `config` file to suit your needs. If you are using this repo's `make`, please name the `Host` `rds`

Then, run from a separate shell:

```bash
make port_forward_rds
```

It's possible to confirm that everything is working with `pgcli`:

```bash
pgcli -h 127.0.0.1 -p 5439 -u dagster -d dev
```

You need to make sure the database you are working with exists. If the database does not exist, please follow AWS [docs](https://docs.aws.amazon.com/redshift/latest/dg/r_CREATE_DATABASE.html) on how to create a database with the necessary configuration. While using `pgcli`, running a command like `create database staging;` should be sufficient.

#### Troubleshoot

If Redshift cluster is not reachable, try reach it from the bastion:

```
sudo yum install telnet
telnet <hostname> <port>
```

## Running jobs

Configuration should be done via env vars. If needed, Dagit UI allow us to override any value.

Each resource has its own set of configuration fields.

Values set from the UI take precedence over the env vars settings. The env var settings appear as a default in the UI.

### scrape_gitlab_metrics_to_s3_job

Uses the following resources:

- `Database`
- `GitLabInterface`
- `S3Interface`
- `TmpFolder`

Needs the following env vars:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_DEFAULT_REGION`
- `AWS_BUCKET_NAME`
- `GITLAB_URL`
- One of, in order of priority, `GITLAB_PRIVATE_TOKEN`, `GITLAB_OAUTH_TOKEN` or `GITLAB_JOB_TOKEN`
- `GITLAB_PROJECTS_IDS` (comma separated string of ints)
- `GITLAB_OWNED` (do you own the projects you specified in `GITLAB_PROJECTS_IDS`)
- `GITLAB_ALLOWED_METRICS` (possible values: `pipeline`)
- `REDSHIFT_USERNAME`
- `REDSHIFT_PASSWORD`
- `REDSHIFT_ENGINE`
- `REDSHIFT_HOST`
- `REDSHIFT_PORT`
- `REDSHIFT_DB_CLUSTER_IDENTIFIER`
- `REDSHIFT_DATABASE`

## GitLab Docs

### Pipelines

[https://docs.gitlab.com/ee/api/pipelines.html](https://docs.gitlab.com/ee/api/pipelines.html)

```
{
  "id": 46,
  "iid": 11,
  "project_id": 1,
  "status": "success",
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "before_sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "created_at": "2016-08-11T11:28:34.085Z",
  "updated_at": "2016-08-11T11:32:35.169Z",
  "started_at": null,
  "finished_at": "2016-08-11T11:32:35.145Z",
  "committed_at": null,
  "duration": 123.65,
  "queued_duration": 0.010,
  "coverage": "30.0",
  "web_url": "https://example.com/foo/bar/pipelines/46"
}
```
