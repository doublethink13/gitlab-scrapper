FROM python:3.10.6

ENV APP_HOME="/opt/gitlab_scrapper"
ENV DAGSTER_HOME="${APP_HOME}/dagster"
WORKDIR $APP_HOME

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get clean -y

COPY requirements.txt .
RUN pip install -r requirements.txt && \
    pip cache purge

COPY dagster dagster
COPY app app

EXPOSE 3000
ENTRYPOINT [ "dagit", "-f", "app/definitions/scrapping.py", "-h", "0.0.0.0", "-p", "3000" ]