resource "aws_redshift_cluster" "main" {
  allow_version_upgrade     = false
  apply_immediately         = false
  cluster_identifier        = "${var.cluster_identifier}-${var.env}"
  cluster_subnet_group_name = aws_redshift_subnet_group.main.name
  cluster_type              = var.cluster_type
  cluster_version           = var.cluster_version
  encrypted                 = var.encrypted
  master_password           = var.master_password
  master_username           = var.master_username
  node_type                 = var.node_type
  number_of_nodes           = var.number_of_nodes
  publicly_accessible       = false
  skip_final_snapshot       = true
  vpc_security_group_ids    = var.vpc_security_group_ids
}

resource "aws_redshift_subnet_group" "main" {
  name       = var.cluster_identifier
  subnet_ids = var.subnet_ids
}
