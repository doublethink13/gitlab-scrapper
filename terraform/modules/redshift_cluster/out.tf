output "copy_from_s3_arn_role" {
  value = aws_iam_role.allow_redshift_s3_copy[0].arn
}

output "redshift_private_endpoint" {
  value = aws_redshift_cluster.main.endpoint
}
