variable "bucket_name" {
  type = string
}

variable "cluster_identifier" {
  type = string
}

variable "cluster_type" {
  default = "single-node"
}

variable "cluster_version" {
  default = "1.0"
}

variable "encrypted" {
  default = false
}

variable "env" {
  type = string
}

variable "master_password" {
  type = string
}

variable "master_username" {
  type = string
}

variable "node_type" {
  default = "dc2.large"
}

variable "number_of_nodes" {
  default = 1
}

variable "subnet_ids" {
  type    = list(string)
  default = []
}

variable "vpc_security_group_ids" {
  type    = list(string)
  default = []
}
