resource "aws_redshift_cluster_iam_roles" "main" {
  cluster_identifier = aws_redshift_cluster.main.cluster_identifier
  iam_role_arns      = [aws_iam_role.allow_redshift_s3_copy[0].arn]
}

resource "aws_iam_role" "allow_redshift_s3_copy" {
  count = length(var.bucket_name) > 0 ? 1 : 0

  name = "allow_redshift_s3_copy_${var.env}"

  assume_role_policy = jsonencode(
    {
      "Version" = "2012-10-17",
      "Statement" = [
        {
          "Effect" = "Allow",
          "Principal" = {
            "Service" = "redshift.amazonaws.com"
          },
          "Action" = "sts:AssumeRole"
        }
      ]
  })

  managed_policy_arns = [aws_iam_policy.allow_redshift_s3_copy[0].arn]
}

resource "aws_iam_policy" "allow_redshift_s3_copy" {
  count = length(var.bucket_name) > 0 ? 1 : 0

  name = "allow_redshift_s3_copy_${var.env}"

  policy = jsonencode({
    "Version" = "2012-10-17",
    "Statement" = [
      {
        "Action" = [
          "s3:ListBucket"
        ],
        "Effect"   = "Allow",
        "Resource" = "arn:aws:s3:::${var.bucket_name}"
      },
      {
        "Action" = [
          "s3:GetObject"
        ],
        "Effect"   = "Allow",
        "Resource" = "arn:aws:s3:::${var.bucket_name}/*"
      }
    ]
  })
}
