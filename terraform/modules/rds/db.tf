resource "aws_db_instance" "main" {
  allocated_storage      = var.allocated_storage
  apply_immediately      = true
  db_name                = "${var.db_name}${var.env}"
  db_subnet_group_name   = aws_db_subnet_group.main.name
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  username               = var.username
  password               = var.password
  port                   = var.port
  skip_final_snapshot    = true
  storage_type           = "gp2"
  vpc_security_group_ids = var.vpc_security_group_ids
}

resource "aws_db_subnet_group" "main" {
  name       = "${var.db_name}-${var.env}"
  subnet_ids = var.subnet_ids
}
