variable "allocated_storage" {
  default = 20
}

variable "db_name" {
  type = string
}

variable "engine" {
  default = "postgres"
}

variable "engine_version" {
  default = "15.2"
}

variable "env" {
  default = "dev"
}

variable "instance_class" {
  default = "db.t3.micro"
}

variable "username" {
  type = string
}

variable "password" {
  type = string
}

variable "port" {
  default = 5432
}

variable "subnet_ids" {
  type = list(string)
}

variable "vpc_security_group_ids" {
  type    = list(string)
  default = []
}
