resource "aws_s3_bucket_acl" "raw_data" {
  bucket = aws_s3_bucket.raw_data.id
  acl    = "private"
}
