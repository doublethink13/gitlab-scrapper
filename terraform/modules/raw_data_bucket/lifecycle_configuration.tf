resource "aws_s3_bucket_lifecycle_configuration" "raw_data" {
  bucket = aws_s3_bucket.raw_data.id

  rule {
    id = "object-expiration"

    dynamic "expiration" {
      for_each = var.object_expiration_in_days > 0 ? { expiration_enabled : true } : {}

      content {
        days = var.object_expiration_in_days
      }
    }

    status = "Enabled"
  }
}
