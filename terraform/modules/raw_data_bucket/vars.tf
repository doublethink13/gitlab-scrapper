variable "bucket_name" {
  type = string
}

variable "env" {
  default = "dev"
}

variable "object_expiration_in_days" {
  default = 7
}
