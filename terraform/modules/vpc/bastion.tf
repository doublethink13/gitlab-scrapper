resource "aws_instance" "bastion" {
  ami                    = var.bastion_ami
  instance_type          = var.bastion_instance_type
  subnet_id              = aws_subnet.public[var.public_subnets[0].cidr_block].id
  user_data_base64       = data.template_cloudinit_config.bastion.rendered
  vpc_security_group_ids = [aws_security_group.ssh.id, aws_security_group.all_out.id]
}

resource "aws_eip" "bastion" {
  instance = aws_instance.bastion.id
  vpc      = true
}

data "template_file" "bastion" {
  template = file("${path.module}/bastion_cloud_init.yml")

  vars = {
    ssh_keys = yamlencode(var.bastion_ssh_keys)
  }
}

data "template_cloudinit_config" "bastion" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.bastion.rendered
  }
}
