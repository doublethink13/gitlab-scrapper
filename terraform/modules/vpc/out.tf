output "allow_bastion_security_group_id" {
  value = aws_security_group.allow_bastion.id
}

output "bation_public_ip" {
  value = aws_eip.bastion.public_ip
}

output "private_subnets_ids" {
  value = [for _, subnet in aws_subnet.private : subnet.id]
}
