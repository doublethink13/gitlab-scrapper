resource "aws_subnet" "public" {
  for_each = { for subnet in var.public_subnets : subnet.cidr_block => subnet }

  vpc_id            = aws_vpc.main.id
  availability_zone = each.value.availability_zone
  cidr_block        = each.value.cidr_block
}

resource "aws_subnet" "private" {
  for_each = { for subnet in var.private_subnets : subnet.cidr_block => subnet }

  vpc_id            = aws_vpc.main.id
  availability_zone = each.value.availability_zone
  cidr_block        = each.value.cidr_block
}
