variable "cidr_block" {
  type = string
}

variable "public_subnets" {
  type = list(object({
    cidr_block        = string
    availability_zone = string
  }))
}

variable "private_subnets" {
  type = list(object({
    cidr_block        = string
    availability_zone = string
  }))
}

variable "bastion_ami" {
  default = "ami-06616b7884ac98cdd"
}

variable "bastion_instance_type" {
  default = "t2.micro"
}

variable "bastion_ssh_keys" {
  type    = list(string)
  default = []
}
