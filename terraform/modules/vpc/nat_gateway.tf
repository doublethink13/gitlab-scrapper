resource "aws_eip" "nat_gateway" {
  depends_on = [aws_internet_gateway.main]
}

resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id     = aws_subnet.private[var.private_subnets[0].cidr_block].id
  depends_on    = [aws_internet_gateway.main]
}
