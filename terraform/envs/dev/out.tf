output "copy_from_s3_arn_role" {
  value = module.redshift_cluster.copy_from_s3_arn_role
}

output "bastion_public_ip" {
  value = module.vpc.bation_public_ip
}

output "rds_private_endpoint" {
  value = module.dagster_storage.rds_endpoint
}

output "rds_port" {
  value = module.dagster_storage.rds_port
}

output "redshift_private_endpoint" {
  value = module.redshift_cluster.redshift_private_endpoint
}
