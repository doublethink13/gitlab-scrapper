variable "env" {
  type = string
}

variable "rds_password" {
  type = string
}

variable "redshift_master_password" {
  type = string
}
