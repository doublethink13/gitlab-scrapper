terraform {
  backend "s3" {
    bucket = "gitlab-scrapper-tfstate"
    key    = "gitlab-scrapper.dev.tfstate"
    region = "eu-central-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.56.0"
    }

    cloudinit = {
      source  = "hashicorp/cloudinit"
      version = "2.3.2"
    }

    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
  }

  required_version = "1.3.9"
}

provider "aws" {
  region = "eu-central-1"

  default_tags {
    tags = {
      Project   = "gitlab-scrapper"
      Terraform = "true"
      Env       = "dev"
    }
  }
}
