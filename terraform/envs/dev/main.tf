# TODO: rename module to s3_bucket
module "raw_data_bucket" {
  source = "../../modules/raw_data_bucket"

  bucket_name = "gitlab-scrapper-raw-data"
  env         = var.env
}

module "vpc" {
  source = "../../modules/vpc"

  cidr_block = "10.0.0.0/16"

  public_subnets = [{
    cidr_block : "10.0.0.0/18"
    availability_zone : "eu-central-1a"
  }]

  private_subnets = [
    {
      cidr_block : "10.0.128.0/18"
      availability_zone : "eu-central-1b"
    },
    {
      cidr_block : "10.0.192.0/18"
      availability_zone : "eu-central-1c"
    }
  ]

  bastion_ssh_keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJBhanWnfoYnGymlsmyJvPnfG+ecFekFIBcujHO9Zsyf bastion"]
}

module "dagster_storage" {
  source = "../../modules/rds"

  db_name                = "dagster"
  env                    = var.env
  password               = var.rds_password
  subnet_ids             = module.vpc.private_subnets_ids
  username               = "dagster"
  vpc_security_group_ids = [module.vpc.allow_bastion_security_group_id]
}

module "redshift_cluster" {
  source = "../../modules/redshift_cluster"

  bucket_name            = module.raw_data_bucket.bucket_name
  cluster_identifier     = "gitlab-scrapper"
  env                    = var.env
  master_password        = var.redshift_master_password
  master_username        = "dagster"
  subnet_ids             = module.vpc.private_subnets_ids
  vpc_security_group_ids = [module.vpc.allow_bastion_security_group_id]
}
